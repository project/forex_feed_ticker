
 1) Upload this contents of this module into the appropriate /sites/*/modules directory on your server

 2) Visit /admin/build/modules to activate the module

 3) Visit /admin/settings/forexfeed/ticker to configure the number of blocks to make available.

 4) Visit /admin/build/block to configure your blocks display.

 5) Configure each block made available in Step #3.
    - For example, visit /admin/build/block/configure/forex_feed_ticker/0 to configure your first block.
    - You may adjust each blocks display style and the currency pairs/quotes to display.
